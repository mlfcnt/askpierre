import React, { useState } from "react";
import logo from "./logo.svg";
import "./App.css";

function App() {
  const [reponsePierre, setReponsePierre] = useState([]);
  const handleQuestion = () => {
    let reponses = [
      "Ca dépend du contexte",
      "Es tu sur de vraiment vouloir faire cela ? ",
      "As tu cherché dans la doc ?"
    ];
    setReponsePierre(reponses[Math.floor(Math.random() * reponses.length)]);
  };

  return (
    <div className="App">
      <h1>Pose ta question à Pierre</h1>
      <img
        src="https://pbs.twimg.com/profile_images/799633638031818752/lPvkecG3.jpg"
        alt=""
      />
      <br />
      <button onClick={() => handleQuestion()}>Question</button>
      <h2>{reponsePierre}</h2>
    </div>
  );
}

export default App;
